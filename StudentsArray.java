import java.util.Random;

public class StudentsArray {
	static int FindTotal(int[] marks) {
		int total = marks[0];
		for (int mark : marks) {
			total += mark;
		}
		return total;
	}

	public static int findmax(int[] marks) {
		int max = marks[0];
		for (int mark : marks) {
			if (mark > max) {
				max = mark;
			}

		}
		return max;
	}

	public static int findmin(int[] marks) {
		int min = marks[0];
		for (int mark : marks) {
			if (mark < min) {
				min = mark;
			}
		}
		return min;

	}

	public static double findAve(int[] marks) {
		return FindTotal(marks) / Double.parseDouble(marks.length + "");
	}
	public static int[]CreateRandommarks(int size,int range){
		Random ran=new Random();
		int[]RanArray=new int [size];
		for(int i= 0; i<size; i++) {
			RanArray[i]=ran.nextInt(range);
		}
		return RanArray;
	}

}
