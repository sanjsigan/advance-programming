
public class StudentsArray2 {
	public static void main(String[] args) {

		int[] AnneMarks = { 75, 90, 100, 78, 100 };
		int[] BredMarks = { 100, 95, 90, 80, 95 };
		int[] LeeMarks = { 95, 100, 90, 80, 70 };
		int[] KumarMarks = { 90, 90, 75, 80, 70 };
		int[] RaviMarks = { 80, 85, 80, 75, 70 };

		System.out.println("Total of Anne marks is :" + StudentsArray.FindTotal(AnneMarks));
		System.out.println("maximum of Anne marks is :" + StudentsArray.findmax(AnneMarks));
		System.out.println("Minimum of Anne marks is :" + StudentsArray.findmin(AnneMarks));
		System.out.println("Average of Anne marks is :" + StudentsArray.findAve(AnneMarks));
		System.out.println("*-------------------------*");

		System.out.println("Total of bred marks is :" + StudentsArray.FindTotal(BredMarks));
		System.out.println("maximum of bred marks is :" + StudentsArray.findmax(BredMarks));
		System.out.println("Minimum of bred marks is :" + StudentsArray.findmin(BredMarks));
		System.out.println("Average of bred marks is :" + StudentsArray.findAve(BredMarks));
		System.out.println("*-------------------------*");

		System.out.println("Total of Lee marks is :" + StudentsArray.FindTotal(LeeMarks));
		System.out.println("maximum of Lee marks is :" + StudentsArray.findmax(LeeMarks));
		System.out.println("Minimum of Lee marks is :" + StudentsArray.findmin(LeeMarks));
		System.out.println("Average of Lee marks is :" + StudentsArray.findAve(LeeMarks));
		System.out.println("*-----------------------------*");

		System.out.println("Total of Kumar marks is :" + StudentsArray.FindTotal(KumarMarks));
		System.out.println("maximum of Kumar  marks is :" + StudentsArray.findmax(KumarMarks));
		System.out.println("Minimum of Kumar  marks is :" + StudentsArray.findmin(KumarMarks));
		System.out.println("Average of Kumar  marks is :" + StudentsArray.findAve(KumarMarks));
		System.out.println("*---------------------------*");

		System.out.println("Total of Ravi marks is :" + StudentsArray.FindTotal(RaviMarks));
		System.out.println("maximum of Ravi marks is :" + StudentsArray.findmax(RaviMarks));
		System.out.println("Minimum of Ravi marks is :" + StudentsArray.findmin(RaviMarks));
		System.out.println("Average of Ravi marks is :" + StudentsArray.findAve(RaviMarks));
		System.out.println("*---------------------------*");

	}
}
